<?php
ini_set("display_errors","On");
error_reporting(E_ALL & ~E_DEPRECATED);
session_start();
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."Secure_PhoneBook_108205".DIRECTORY_SEPARATOR."vendor/autoload.php");
use \App\BITM\SEIP108205\PhoneBook\PhoneBook;
use \App\BITM\SEIP108205\Message\Message;
use \App\BITM\SEIP108205\Utility\Utility;

$book = new PhoneBook();
$theBook = $book->edit($_GET['id']);

?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Edit an Item</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div style="text-align:center;">
            <H1>Edit an Item:</H1>
        <form action="./update.php" method="post">
            <fieldset>
                <legend>Edit PhoneBook Info</legend>
                <input type="text" name="id" value="<?php echo $theBook->id ?>"/>
                
                
                    <label>phoneBook Name:</label>
                    <input autofocus="autofocus" 
                    
                           placeholder="Enter the name of phonebook" 
                           type="text" 
                           name="name"
                     
                           required="required"
                           value="<?php echo $theBook->name ?>"
                      
                           />
                    <label> Phonebook Number:</label>
                    <input autofocus="autofocus" 
                    
                           placeholder="Enter the number of phonebook" 
                           type="text" 
                           name="number"
                     
                           required="required"
                           value="<?php echo $theBook->number?>"
                      
                           />
                 
                
                <button  type="submit">Save</button>
                <button  type="submit">Save & Add Again</button>
<!--                <input type="submit" value="Save" />-->
                <input type="reset" value="Reset" />
            </fieldset>
        </form> 
    </div>
        <nav>
            <li><a href="index.php">Go to List</a></li>
        </nav>
    </body>
</html>

