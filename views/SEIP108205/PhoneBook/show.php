<?php
ini_set("display_errors","On");
error_reporting(E_ALL & ~E_DEPRECATED);
session_start();
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."Secure_PhoneBook_108205".DIRECTORY_SEPARATOR."vendor/autoload.php");
use \App\BITM\SEIP108205\PhoneBook\PhoneBook;
use \App\BITM\SEIP108205\Message\Message;
use \App\BITM\SEIP108205\Utility\Utility;

$book=new PhoneBook();
$b=$book->show($_GET['id']);
//Utility::dd($b);
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <h1><?php echo $b->name;?></h1>
        <dl>
             <dt>ID</dt>
            <dd><?php echo $b->id;?></dd> 
            <dt>Phone</dt>
            <dd><?php echo $b->number;?></dd>
        </dl>
        
        <nav>
            <li><a href="index.php">Go to List</a></li>
        </nav>
    </body>
</html>
